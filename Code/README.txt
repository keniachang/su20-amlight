# 2020SUM-Amlight/of_core


of_core is a fork of
https://github.com/kytos/of_core


The fork can be located at
https://github.com/2020SUM-Amlight/of_core


A user manual, and installation guide can be found in the Appendix D
of the project documentation and in the directory with this README.txt file.


We modified the following files:


 - kytos/of_core/flow.py
 - kytos/of_core/v0x04/match_fields.py
 - kytos/of_core/v0x04/utils.py


|   .coveragerc
|   .gitignore
|   .travis.yml
|   AUTHORS.rst
|   CHANGELOG.rst
|   flow.py
|   kytos.json
|   LICENSE
|   main.py
|   MANIFEST.in
|   README.rst
|   settings.py
|   setup.cfg
|   setup.py
|   tox.ini
|   utils.py
|   __init__.py
|
+---requirements
|       dev.in
|       dev.txt
|       run.in
|
+---tests
|   |   helpers.py
|   |   __init__.py
|   |
|   +---integration
|   |   |   test_action.py
|   |   |   test_flow.py
|   |   |   test_main.py
|   |   |   test_match.py
|   |   |   __init__.py
|   |   |
|   |   \---v0x04
|   |           test_match.py
|   |           __init__.py
|   |
|   \---unit
|       |   test_flow.py
|       |   test_main.py
|       |   test_match.py
|       |   test_utils.py
|       |   __init__.py
|       |
|       +---v0x01
|       |       test_utils.py
|       |       __init__.py
|       |
|       \---v0x04
|               test_utils.py
|               __init__.py
|
+---v0x01
|       flow.py
|       utils.py
|       __init__.py
|
\---v0x04
        flow.py
        match_fields.py
        utils.py
        __init__.py


9 directories, 45 files


# 2020SUM-Amlight/python-openflow


python-openflow is a fork of
https://github.com/kytos/python-openflow


The fork can be located at
https://github.com/2020SUM-Amlight/python-openflow


We modified the following files:
 - kytos/python-openflow/pyof/foundation/basic_types.py
 - kytos/python-openflow/pyof/foundation/network_types.py
 - kytos/python-openflow/tests/unit/test_foundation/test_network_types.py


|   .coveragerc
|   .gitignore
|   .scrutinizer.yml
|   .travis.yml
|   AUTHORS.rst
|   CHANGELOG.rst
|   LICENSE
|   Makefile
|   README.rst
|   setup.cfg
|   setup.py
|   tox.ini
|
+---.github
|   \---workflows
|           pythonpublish.yml
|
+---docs
|   |   .gitignore
|   |   AUTHORS.rst
|   |   conf.py
|   |   index.rst
|   |   LICENSE.rst
|   |   Makefile
|   |   references.rst
|   |
|   +---examples
|   |       index.rst
|   |       packing.rst
|   |       unpacking.rst
|   |
|   \---_static
|           .keep
|
+---pyof
|   |   utils.py
|   |   __init__.py
|   |
|   +---foundation
|   |       base.py
|   |       basic_types.py
|   |       constants.py
|   |       exceptions.py
|   |       network_types.py
|   |       __init__.py
|   |
|   +---v0x01
|   |   |   __init__.py
|   |   |
|   |   +---asynchronous
|   |   |       error_msg.py
|   |   |       flow_removed.py
|   |   |       packet_in.py
|   |   |       port_status.py
|   |   |       __init__.py
|   |   |
|   |   +---common
|   |   |       action.py
|   |   |       constants.py
|   |   |       flow_match.py
|   |   |       header.py
|   |   |       phy_port.py
|   |   |       queue.py
|   |   |       utils.py
|   |   |       __init__.py
|   |   |
|   |   +---controller2switch
|   |   |       barrier_reply.py
|   |   |       barrier_request.py
|   |   |       common.py
|   |   |       features_reply.py
|   |   |       features_request.py
|   |   |       flow_mod.py
|   |   |       get_config_reply.py
|   |   |       get_config_request.py
|   |   |       packet_out.py
|   |   |       port_mod.py
|   |   |       queue_get_config_reply.py
|   |   |       queue_get_config_request.py
|   |   |       set_config.py
|   |   |       stats_reply.py
|   |   |       stats_request.py
|   |   |       __init__.py
|   |   |
|   |   \---symmetric
|   |           echo_reply.py
|   |           echo_request.py
|   |           hello.py
|   |           vendor_header.py
|   |           __init__.py
|   |
|   \---v0x04
|       |   __init__.py
|       |
|       +---asynchronous
|       |       error_msg.py
|       |       flow_removed.py
|       |       packet_in.py
|       |       port_status.py
|       |       __init__.py
|       |
|       +---common
|       |       action.py
|       |       constants.py
|       |       flow_instructions.py
|       |       flow_match.py
|       |       header.py
|       |       port.py
|       |       queue.py
|       |       utils.py
|       |       __init__.py
|       |
|       +---controller2switch
|       |       barrier_reply.py
|       |       barrier_request.py
|       |       common.py
|       |       features_reply.py
|       |       features_request.py
|       |       flow_mod.py
|       |       get_async_reply.py
|       |       get_async_request.py
|       |       get_config_reply.py
|       |       get_config_request.py
|       |       group_mod.py
|       |       meter_mod.py
|       |       multipart_reply.py
|       |       multipart_request.py
|       |       packet_out.py
|       |       port_mod.py
|       |       queue_get_config_reply.py
|       |       queue_get_config_request.py
|       |       role_reply.py
|       |       role_request.py
|       |       set_async.py
|       |       set_config.py
|       |       table_mod.py
|       |       __init__.py
|       |
|       \---symmetric
|               echo_reply.py
|               echo_request.py
|               experimenter.py
|               hello.py
|               __init__.py
|
+---raw
|   |   README.rst
|   |
|   +---v0x01
|   |       ofpt_aggregate_stats_reply.dat
|   |       ofpt_aggregate_stats_request.dat
|   |       ofpt_barrier_reply.dat
|   |       ofpt_barrier_request.dat
|   |       ofpt_desc_stats_reply.dat
|   |       ofpt_echo_reply.dat
|   |       ofpt_echo_request.dat
|   |       ofpt_error_msg.dat
|   |       ofpt_features_reply.dat
|   |       ofpt_features_request.dat
|   |       ofpt_flow_add.dat
|   |       ofpt_flow_delete.dat
|   |       ofpt_flow_removed.dat
|   |       ofpt_flow_stats_reply.dat
|   |       ofpt_flow_stats_request.dat
|   |       ofpt_get_config_reply.dat
|   |       ofpt_get_config_request.dat
|   |       ofpt_hello.dat
|   |       ofpt_packet_in.dat
|   |       ofpt_packet_out.dat
|   |       ofpt_port_mod.dat
|   |       ofpt_port_stats.dat
|   |       ofpt_port_stats_request.dat
|   |       ofpt_port_status.dat
|   |       ofpt_queue_get_config_reply.dat
|   |       ofpt_queue_get_config_request.dat
|   |       ofpt_queue_stats.dat
|   |       ofpt_queue_stats_request.dat
|   |       ofpt_set_config.dat
|   |       ofpt_stats_reply.dat
|   |       ofpt_stats_request.dat
|   |       ofpt_table_stats.dat
|   |       ofpt_vendor_header.dat
|   |       ofpt_vendor_stats_reply.dat
|   |
|   \---v0x04
|           ofpt_aggregate_stats.dat
|           ofpt_aggregate_stats_request.dat
|           ofpt_barrier_reply.dat
|           ofpt_barrier_request.dat
|           ofpt_echo_reply.dat
|           ofpt_echo_request.dat
|           ofpt_error.dat
|           ofpt_features_reply.dat
|           ofpt_features_request.dat
|           ofpt_flow_mod.dat
|           ofpt_flow_removed.dat
|           ofpt_flow_stats.dat
|           ofpt_flow_stats_request.dat
|           ofpt_group_stats.dat
|           ofpt_group_stats_request.dat
|           ofpt_hello.dat
|           ofpt_meter_multipart_request.dat
|           ofpt_packet_in.dat
|           ofpt_packet_out.dat
|           ofpt_port_desc.dat
|           ofpt_port_status.dat
|           ofpt_set_config.dat
|
+---requirements
|       dev.in
|       dev.txt
|
\---tests
    |   __init__.py
    |
    \---unit
        |   raw_dump.py
        |   test_struct.py
        |   test_utils.py
        |   __init__.py
        |
        +---test_class_inheritance
        |       test_inheritance.py
        |       __init__.py
        |
        +---test_foundation
        |       test_base.py
        |       test_basic_types.py
        |       test_network_types.py
        |       __init__.py
        |
        +---v0x01
        |   |   __init__.py
        |   |
        |   +---test_asynchronous
        |   |       test_error_msg.py
        |   |       test_flow_removed.py
        |   |       test_packet_in.py
        |   |       test_port_status.py
        |   |       __init__.py
        |   |
        |   +---test_common
        |   |       test_action.py
        |   |       test_flow_match.py
        |   |       test_header.py
        |   |       test_phy_port.py
        |   |       test_queue.py
        |   |       __init__.py
        |   |
        |   +---test_controller2switch
        |   |       test_aggregate_stats_reply.py
        |   |       test_aggregate_stats_request.py
        |   |       test_barrier_reply.py
        |   |       test_barrier_request.py
        |   |       test_desc_stats.py
        |   |       test_features_reply.py
        |   |       test_features_request.py
        |   |       test_flow_mod.py
        |   |       test_flow_stats.py
        |   |       test_flow_stats_request.py
        |   |       test_get_config_reply.py
        |   |       test_get_config_request.py
        |   |       test_packet_out.py
        |   |       test_port_mod.py
        |   |       test_port_stats.py
        |   |       test_port_stats_request.py
        |   |       test_queue_get_config_reply.py
        |   |       test_queue_get_config_request.py
        |   |       test_queue_stats.py
        |   |       test_queue_stats_request.py
        |   |       test_set_config.py
        |   |       test_stats_reply.py
        |   |       test_stats_request.py
        |   |       test_table_stats.py
        |   |       test_vendor_stats.py
        |   |       __init__.py
        |   |
        |   \---test_symmetric
        |           test_echo_reply.py
        |           test_echo_request.py
        |           test_hello.py
        |           test_vendor_header.py
        |           __init__.py
        |
        \---v0x04
            |   test_struct.py
            |   __init__.py
            |
            +---test_asynchronous
            |       test_error_msg.py
            |       test_flow_removed.py
            |       test_packet_in.py
            |       test_port_status.py
            |       __init__.py
            |
            +---test_common
            |       test_flow_match.py
            |       test_port.py
            |       test_queue.py
            |       __init__.py
            |
            +---test_controller2switch
            |       test_aggregate_stats.py
            |       test_aggregate_stats_request.py
            |       test_barrier_reply.py
            |       test_barrier_request.py
            |       test_features_reply.py
            |       test_features_request.py
            |       test_flow_mod.py
            |       test_flow_stats.py
            |       test_flow_stats_request.py
            |       test_get_async_reply.py
            |       test_get_async_request.py
            |       test_get_config_reply.py
            |       test_get_config_request.py
            |       test_group_mod.py
            |       test_group_stats.py
            |       test_group_stats_request.py
            |       test_meter_mod.py
            |       test_meter_multipart_request.py
            |       test_multipart_reply.py
            |       test_multipart_request.py
            |       test_packet_out.py
            |       test_port_desc.py
            |       test_port_mod.py
            |       test_port_stats.py
            |       test_port_stats_request.py
            |       test_queue_get_config_reply.py
            |       test_queue_get_config_request.py
            |       test_queue_stats.py
            |       test_queue_stats_request.py
            |       test_role_reply.py
            |       test_role_request.py
            |       test_set_async.py
            |       test_set_config.py
            |       test_table_mod.py
            |       test_table_stats.py
            |       __init__.py
            |
            \---test_symmetric
                    test_echo_reply.py
                    test_echo_request.py
                    test_hello.py
                    test_vendor_header.py
                    __init__.py


35 directories, 279 files